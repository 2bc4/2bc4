## SSH public signing key
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJitCrndwqpmYkKUUAAUYwtm+jUsuZM1HH+Et8EBlgVO

- [2bc4.pub](https://gitlab.com/2bc4/2bc4/-/blob/master/2bc4.pub)
- [allowed_signers](https://gitlab.com/2bc4/2bc4/-/blob/master/allowed_signers)
- [GitHub mirror](https://github.com/2bc4/2bc4)
